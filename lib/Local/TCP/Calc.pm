package Local::TCP::Calc;

use strict;
use warnings;

use DDP;

use CBOR::XS qw(encode_cbor decode_cbor);

use Exporter 'import';
our @EXPORT_OK = qw(pack_header unpack_header pack_message unpack_message
    TYPE_START_WORK TYPE_CHECK_WORK TYPE_CONN_ERR TYPE_CONN_OK TYPE_CONN_ESTAB
        TYPE_WORK_ID_ERR TYPE_NEW_WORK_ERR TYPE_NEW_WORK_OK TYPE_UNKNOWN
    STATUS_NEW STATUS_WORK STATUS_DONE STATUS_ERROR
);
our %EXPORT_TAGS = (
    FUNCS => [qw(pack_header unpack_header pack_message unpack_message)],
    TYPES => [qw(TYPE_START_WORK TYPE_CHECK_WORK TYPE_CONN_ERR TYPE_CONN_OK
        TYPE_CONN_ESTAB TYPE_WORK_ID_ERR TYPE_NEW_WORK_ERR TYPE_NEW_WORK_OK
        TYPE_UNKNOWN
    )],
    STATS => [qw(STATUS_NEW STATUS_WORK STATUS_DONE STATUS_ERROR)]
);

sub TYPE_START_WORK     {1}
sub TYPE_CHECK_WORK     {2}
sub TYPE_CONN_ERR       {3}
sub TYPE_CONN_OK        {4}
sub TYPE_CONN_ESTAB     {5}
sub TYPE_NEW_WORK_OK    {6}
sub TYPE_NEW_WORK_ERR   {0}
sub TYPE_WORK_ID_ERR    {0}
sub TYPE_UNKNOWN        {255}

sub STATUS_NEW   {1}
sub STATUS_WORK  {2}
sub STATUS_DONE  {3}
sub STATUS_ERROR {4}

# Packs type and size of message bypassed by value
sub pack_header {
    my $type = shift;
    my $size = shift;
    return pack("Cs", $type, $size)
}

# Returns ref to hash
sub unpack_header {
    my $header = shift;
    my ($type, $size) = unpack("Cs", $header);
    return {type => $type, size => $size};
}

# Packs messages bypassed to encoding function by ref to array
sub pack_message {
    my $messages = shift;
    return pack("(C/a*)*", @{$messages})
}

# Returns ref to messages
sub unpack_message {
    my $message = shift;
    return [unpack(("(C/a*)*"), $message)]
}

1;
