package Local::TCP::Calc::SocketSubs;

use strict;
use warnings;

use DDP;

our $VERSION = '0.0.1';

use Local::TCP::Calc qw(:FUNCS);

use Exporter 'import';
our @EXPORT_OK = qw(sendm senda sendid sendh receive close_connect
    $HEADER_BYTES
    $SEND_SUCCESS
    $HEADER_ERROR
    $MESSAGE_ERROR
);
our %EXPORT_TAGS = (
    FUNCS => [qw(sendm senda sendid receive close_connect)],
    CONST => [qw($HEADER_BYTES $SEND_SUCCESS $HEADER_ERROR $MESSAGE_ERROR)]
);

our $HEADER_BYTES   = 3;

our $SEND_SUCCESS   = 0;

our $HEADER_ERROR   = 1;
our $MESSAGE_ERROR  = 2;

sub sendm {
    my $socket = shift;
    my $type = shift;
    my $message_input = shift;
    my $message;

    if (ref($message_input) eq "ARRAY") {
        $message = pack_message($message_input);
    }
    else {
        $message = pack_message([$message_input]);
    }

    my $header = pack_header($type, length($message));
    if (syswrite($socket, $header) != $HEADER_BYTES) {
        close_connect($socket. {ERROR => "Wrong header length while sending, close connection...\n"});
        return $HEADER_ERROR
    }
    if (syswrite($socket, $message) != length($message)) {
        close_connect($socket, {ERROR => "Wrong message length, close connection...\n"});
        return $MESSAGE_ERROR;
    }
    return $SEND_SUCCESS
}

sub senda {
    my $socket = shift;
    my $message = pack_message(shift);

    if (syswrite($socket, $message) != length($message)) {
        close_connect($socket, {ERROR => "Wrong message length, close connection...\n"});
        return $MESSAGE_ERROR;
    }
    return $SEND_SUCCESS
}

sub sendid {
    my $socket = shift;
    my $message_input = shift;
    my $message = pack_message([$message_input]);

    if (syswrite($socket, $message) != length($message)) {
        close_connect($socket, {ERROR => "Wrong message length, close connection...\n"});
        return $MESSAGE_ERROR;
    }
    return $SEND_SUCCESS
}

sub receive {
    my $socket = shift;
    my $header_raw;
    my $header_ref;
    my $message_raw;
    my $readed = sysread($socket, $header_raw, $HEADER_BYTES);
    if ($! or $readed != $HEADER_BYTES) {
        close_connect($socket, {ERROR => "Wrong header length while receiving, close connection...\n"});
        return $HEADER_ERROR;
    }
    $header_ref = unpack_header($header_raw);

    if (sysread($socket, $message_raw, $header_ref->{size}) != $header_ref->{size}) {
        close_connect($socket, {ERROR => "Wrong message length, close connection...\n"});
        return $MESSAGE_ERROR;
    }

    return {type => $header_ref->{type}, messages => unpack_message($message_raw)};
}


sub close_connect {
    my $socket = shift;
    my $info_hash = shift;
    if ($info_hash->{ERROR}) {
        print STDERR $info_hash->{ERROR}
    }
    close($socket)
}

1;
