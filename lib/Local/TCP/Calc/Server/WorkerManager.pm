package Local::TCP::Calc::Server::WorkerManager;

use strict;
use warnings;

use POSIX;

use Local::TCP::Calc::Server::Queue;
use Local::TCP::Calc::Server::Worker;

use DDP;

my $processing_count    = 0;
my $pids_master         = {};

my $max_worker;
my $check_queue_workers_active = 0;

sub REAPER {
    while (my $pid = waitpid(-1, WNOHANG)) {

        last if $pid == -1;

        if (WIFEXITED($?)) {
            if (exists($pids_master->{$pid})) {
                delete $pids_master->{$pid};
                $processing_count--;
            }
        }
    }
}

sub INT_HANDLER {
    for (%$pids_master) {
        kill('INT', $_)
    }
    exit(0)
}

sub ALRM_HANDLER {
    $check_queue_workers_active++;
};

sub start {
    my $pkg = shift;
    my $queue = shift;
    $max_worker = shift;
    my $max_forks_per_task = shift;


    $SIG{INT} = \&INT_HANDLER;
    $SIG{ALRM} = \&ALRM_HANDLER;

    # The Big Loop
    while (1) {
        if ($check_queue_workers_active && $processing_count < $max_worker) {
            my $task_id = $queue->get;

            my $child = fork();

            # --- Parent process part
            if ($child) {
                $SIG{CHLD} = \&REAPER;
                $processing_count++;
                $check_queue_workers_active--;
                $pids_master->{$child} = $child;
                next
            }

            # --- Child process part
            elsif ($child == 0) {
                my $worker = Local::TCP::Calc::Server::Worker->new(
                    cur_task_id => $task_id,
                    # calc_ref => \&func,
                    max_forks   => $max_forks_per_task,
                    file_prefix => "/tmp/local_".$queue->{port}.":".$task_id."_"
                );

                if ($worker->start) {
                    $queue->to_done($task_id, 'ERROR')
                }
                else {
                    $queue->to_done($task_id, 'DONE')
                }

                exit(0)
            }
            else {
                die "Can't fork!\n"
            }
        }
    }

}

1;
