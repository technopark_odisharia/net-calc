package Local::TCP::Calc::Server::Queue;

use strict;
use warnings;

use diagnostics;
use DDP;

use Mouse;
use Local::TCP::Calc;
use Fcntl qw(:flock);
# use open qw< :encoding(UTF-8) >;
use List::Util qw(min max);

has f_handle            => (is => 'rw', isa => 'FileHandle');
has queue_filename      => (is => 'ro', isa => 'Str', default => '/tmp/local_queue');
has port                => (is => 'ro', isa => 'Int');
has max_tasks           => (is => 'rw', isa => 'Int', default => 0);
has init_string         => (is => 'rw', isa => 'Str', default => "# Empty");
has last_added_id       => (is => 'rw', isa => 'Int', default => 0);

sub init {
    my $self = shift;
    my $port = shift;
    my @monthes = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
    my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime(time);
    $self->{init_string} = sprintf("# Queue at $port port was initialized "
        ."on $monthes[$mon] $mday, @{[ $year + 1900 ]} at $hour\:$min\:$sec\n"
        ."LAST_ADDED:".$self->{last_added_id}."\n");

    CORE::open($self->{f_handle}, '>>', $self->{queue_filename})
        or die "Can't initialize the queue file ".$self->{queue_filename}."\n";
    $self->{f_handle}->print($self->{init_string});

    CORE::close($self->{f_handle});
};

sub open {
    my $self = shift;
    my $open_type = shift;
    CORE::open($self->{f_handle}, $open_type, $self->{queue_filename})
        or die "Can't open the queue file ".$self->{queue_filename}."\n";
    flock($self->{f_handle}, LOCK_EX)
        or die "Can't lock the queue file ".$self->{queue_filename}."\n";

    my $fh = $self->{f_handle};
    chomp($self->{init_string} = <$fh>);
    chomp($_ = <$fh>);
    my @temp = split(/\:/);
    $self->{last_added_id} = $temp[1];

    # print STDERR "Last added id: $self->{last_added_id}\n";

    my %file;

    while (<$fh>) {
        chomp;
        @temp = split(/\:/);
        $file{$temp[0]} = $temp[1]
    }

    return \%file
};

sub close {
    my $self = shift;
    my $open_type = shift;
    my $file_content = shift;

    # rewriting existing file via $file_content
    # print STDERR "Closing the file\n";
    # print STDERR "File $self->{queue_filename} is: \n";
    # p %{$file_content};
    if ($open_type ne '<') {
        seek($self->{f_handle}, 0, 0);
        truncate($self->{f_handle},0);
        my $fh = $self->{f_handle};
        $fh->print("$self->{init_string}\n"
            ."LAST_ADDED:$self->{last_added_id}\n");
        $fh->print("$_:$file_content->{$_}\n") foreach (keys %{$file_content});
    }
    flock($self->{f_handle}, LOCK_UN)
        or die "Can't unlock the queue file ".$self->{queue_filename}."\n";

    CORE::close($self->{f_handle})
        or die "Can't close the queue file ".$self->{queue_filename}."\n";
};

sub to_done {
    my $self = shift;
    my $task_id = shift;
    my $status = shift;

    my $file_ref = $self->open('+<');
    $file_ref->{$task_id} = $status;
    $self->close('+<', $file_ref);

    # Переводим задание в статус DONE, сохраняем имя файла с резуьтатом работы
};

sub get_status {
    my $self = shift;
    my $task_id = shift;

    my $status = $self->open('<')->{$task_id};
    $self->close('<');

    if ($status eq 'DONE' || $status eq 'ERROR') {
        return ($status, '/tmp/local_'.$self->{port}.':'.$task_id.'_result')
    } else {
        return $status
    }
    # Возвращаем статус задания по id, и в случае DONE или ERROR имя файла с результатом
};

sub delete {
    my $self = shift;
    my $task_id = shift;
    # my $status = shift;

    # Удаляем задание из очереди в соответствующем статусе
    my $file = $self->open('+<');
    delete $file->{$task_id};
    unlink('/tmp/local_'.$self->{port}.':'.$task_id.'_'.$_) foreach (@{['task', 'result']});
    $self->close('+<', $file)

};

sub delete_all {
    # Delete all tasks
    my $self = shift;

    my $file = $self->open('+<');

    foreach my $task_id (%{$file}) { unlink('/tmp/local_'.$self->{port}.':'.$task_id.'_'.$_) foreach (@{['task', 'result']}) }

    $self->close('+<', $file);
    unlink($self->{queue_filename});
};

sub get {
    my $self = shift;

    my $file = $self->open('+<');
    my @new_works;

    foreach (keys %{$file}) {
        push(@new_works, $_) if ($file->{$_} eq 'NEW')
    }
    my $task_id = min(@new_works);
    $file->{$task_id} = 'WORK' if $task_id;

    $self->close('+<', $file);
    return $task_id;    # returns undef or id of task;
};

sub add {
    my $self = shift;
    my $new_work = shift;

    # Добавляем новое задание с проверкой, что очередь не переполнилась

    my $file = $self->open("+<");
    my @works_in_queue;
    my $new_task_id = undef;
    foreach (keys %{$file}) { push(@works_in_queue, $_) if ($file->{$_} eq 'NEW' || $file->{$_} eq 'WORK') };

    if (scalar @works_in_queue < $self->{max_tasks}) {
        $self->{last_added_id} += 1;
        $new_task_id = $self->{last_added_id};
        # print STDERR "New task number is $new_task_id\n";
        $file->{$new_task_id} = 'NEW';

        CORE::open(my $fh, '>', '/tmp/local_'.$self->{port}.':'.$new_task_id.'_task')
            or die "Can't open /tmp/local_".$self->{port}.":".$new_task_id."_task\n";

        print $fh join("\n", @{$new_work})."\n";

        CORE::close($fh)
            or die "Can't close /tmp/local_".$self->{port}.":".$new_task_id."_task\n";
    }

    $self->close("+<", $file);
    return $new_task_id;
};

no Mouse;
__PACKAGE__->meta->make_immutable();

1;
