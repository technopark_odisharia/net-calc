package Local::TCP::Calc::Server::Worker;

use strict;
use warnings;
use Mouse;
use Fcntl qw(:flock);
use POSIX;

use Local::Calc::Calc;

use DDP;

has cur_task_id => (is => 'ro', isa => 'Int', required => 1);
# has forks       => (is => 'rw', isa => 'HashRef', default => sub {return {}});
# has calc_ref    => (is => 'ro', isa => 'CodeRef', required => 1);
has max_forks   => (is => 'ro', isa => 'Int', required => 1);
has file_prefix => (is => 'ro', isa => 'Str', required => 1);

my $forks       = {};
my $forks_count = 0;
my $err         = 0;

sub REAPER {
    while (my $pid = waitpid(-1, WNOHANG)) {

        last if $pid == -1;

        if (($? >> 8) != 0) {
            $err++;
        }

        delete $forks->{$pid};
        $forks_count--;

    }
}

sub INT_HANDLER {
    for (%{$forks}) {
        kill('TERM', $_);
    }
    exit(0);
}

sub write_res {
    my $self = shift;
    my $task = shift;
    my $res = shift;;
    # Записываем результат выполнения задания
    open(my $fh, '>>', $self->{file_prefix}."result")
        or die $self->{file_prefix}."result\n";
    flock($fh, LOCK_EX)
        or die $self->{file_prefix}."result\n";
    seek($fh, 0,2);
    print $fh "$task == $res\n";

    flock($fh, LOCK_UN)
        or die $self->{file_prefix}."result\n";
    close($fh)
        or die $self->{file_prefix}."result\n";
}

sub start {
    my $self = shift;
    my $task = shift;

    my $status;

    $SIG{INT} = \&INT_HANDLER;
    open(my $fh_task, '<', $self->{file_prefix}."task")
        or die $self->{file_prefix}."task\n";


    while (<$fh_task>) {
        chomp;
        if ($status) {
            INT_HANDLER;
            last
        }

        sleep 0.05 while $forks_count == $self->{max_forks};

        my $child = fork();

        # --- Parent process part
        if ($child) {
            $SIG{CHLD} = \&REAPER;
            $forks_count++;
            $forks->{$child} = $child;
        }

        # --- Child process part
        elsif ($child == 0) {
            my $res = evaluate(rpn($_));
            $self->write_res($_, $res);
            if ($res eq "NaN") {
                exit(100)
            }
            else {
                exit(0)
            }
        }

        else {
            die "Can't fork!\n"
        }

        sleep 0.05 while $forks_count;
    }

    close $fh_task;
    return $err;

    # Начинаем выполнение задания. Форкаемся на нужное кол-во форков для обработки массива примеров
    # Вызов блокирующий, ждём  пока не завершатся все форки
    # В форках записываем результат в файл, не забываем про локи, чтобы форки друг другу не портили результат
}

no Mouse;
__PACKAGE__->meta->make_immutable();

1;
