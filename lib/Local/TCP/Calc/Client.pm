package Local::TCP::Calc::Client;

use strict;
use IO::Socket::INET;
use Local::TCP::Calc qw(:TYPES :STATS);
use Local::TCP::Calc::SocketSubs qw(:FUNCS :CONST);

use DDP;

sub set_connect {
    my $pkg = shift;
    my $ip = shift;
    my $port = shift;
    my $socket = IO::Socket::INET->new(
        PeerAddr    => $ip,
        PeerPort    => $port,
        Proto       => "tcp",
        Type        => SOCK_STREAM,
        ReuseAddr   => 1,
    ) or die "$!: can't connect to server on $ip:$port\n";

    # my $send_status = sendm($socket, TYPE_CONN_ESTAB, '0');
    # if      ($send_status == $HEADER_ERROR)  { return; }
    # elsif   ($send_status == $MESSAGE_ERROR) { return; }

    my $response = receive($socket);
    if (    $response != $HEADER_ERROR
        &&  $response != $MESSAGE_ERROR
        &&  $response->{type} != TYPE_CONN_ERR()) {
        return $socket
    }
    else {
        die "Connection error..."
    }
}

sub do_request {
    my $pkg = shift;
    my $socket = shift;
    my $type = shift;
    my $message = shift;

    my $send_status = sendm($socket, $type, $message);
    if      ($send_status == $HEADER_ERROR)  { return; }
    elsif   ($send_status == $MESSAGE_ERROR) { return; }

    my $response = receive($socket);
    if      ($response == $HEADER_ERROR)    { return; }
    elsif   ($response == $MESSAGE_ERROR)   { return; }
    elsif   (ref($response) eq "HASH")      {
            my @return_response;
            if ($response->{type} == TYPE_NEW_WORK_OK) {
                @return_response = $response->{messages}->[0];
                return @return_response
            }
            @return_response = ($response->{type});
            push(@return_response, @{$response->{messages}});
            return @return_response
    }

}

1;
