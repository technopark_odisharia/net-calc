package Local::TCP::Calc::Server;

use strict;
use warnings;

use IO::Socket;
use IO::Socket::INET;
use POSIX;

use Local::TCP::Calc qw(:TYPES :STATS);
use Local::TCP::Calc::SocketSubs qw(:FUNCS :CONST);
use Local::TCP::Calc::Server::Queue;
use Local::TCP::Calc::Server::WorkerManager;

use DDP;

my $max_worker;
my $in_process = 0;

my $pids_master         = {};
my $hosts               = [];
my $receiver_count      = 0;
my $max_forks_per_task  = 0;

my $queue;

my $worker_manager_pid;
my $error_pid;

sub REAPER {
    while (my $pid = waitpid(-1, WNOHANG())) {

        last if $pid == -1;

        # if (WIFEXITED($?)) {
            if (exists($pids_master->{$pid})) {
                delete $pids_master->{$pid};
                # print STDERR "Decreasing the number of recievers in PID $$...\n";
                $receiver_count--;
            }
            elsif ($pid == $error_pid) {
                # print STDERR "ERROR: Can't listen any more clients, closing the connection...\n";
            }
        # }
    }
    $SIG{CHLD} = \&REAPER;
};

sub INT_HANDLER {
    kill('INT', $worker_manager_pid);

    for (%{$pids_master}) {
        kill('TERM', $_);
    }
    $queue->delete_all();
    exit(0);
};

$SIG{TERM} = \&INT_HANDLER;

sub error_die {

};

sub start_server {
    my ($pkg, $port, %opts) = @_;
    $max_worker         = $opts{max_worker}         // die "max_worker required";
    $max_forks_per_task = $opts{max_forks_per_task} // die "max_forks_per_task required";
    my $max_receiver    = $opts{max_receiver}       // die "max_receiver required";
    my $max_queue_task  = $opts{max_queue_task}     // die "max_queue_task required";

    my $socket = IO::Socket::INET->new(
        Proto       => 'tcp',
        Type        => SOCK_STREAM,
        LocalPort   => $port,
        LocalAddr   => '127.0.0.1',
        ReuseAddr   => 1,
        Listen      => $max_receiver,
    ) or die "Can't establish connection on port $port\n";

    # warn "Server on port $port was started with PID $$ \n";

    $queue = Local::TCP::Calc::Server::Queue->new(
        max_tasks       => $max_queue_task,
        queue_filename  => '/tmp/local_queue:'.$port,
        port            => $port
    );
    $queue->init($port);

    # -- Worker creating
    $worker_manager_pid = fork();

    if ($worker_manager_pid) {
        $SIG{INT} = \&INT_HANDLER;
    }

    # --- Child process part
    # ---- Starting the worker manager's The Big Loop process
    elsif ($worker_manager_pid == 0) {
        Local::TCP::Calc::Server::WorkerManager->start($queue, $max_worker, $max_forks_per_task);
       exit(0);
    }

    # --- Something went wrong, we cannot fork
    else {
        die "Can't create the worker manager process: $!\n"
    }

    # -- The Big Loop
    while (1) {
        while (my $client = $socket->accept()) {
        # next unless defined($client);
            $client->autoflush(1);

            if ($receiver_count < $max_receiver) {

                # -- Connection management
                my $child_pid = fork();

                # --- Parent process part
                if ($child_pid) {
                    $SIG{CHLD} = \&REAPER;
                    close($client);
                    $receiver_count++;
                    # warn "\n-----------\n".
                    #     "Now we have $receiver_count clients while maximum is $max_receiver clients\nPID: $$\nPort: $port\n".
                    #     "-----------\n";
                    $pids_master->{$child_pid} = $child_pid;
                    next;
                }

                # --- Child process part
                elsif ($child_pid == 0) {
                    exit(1) if sendm($client, TYPE_CONN_OK, ''); # with header OK;\

                    my $msg = receive($client);
                    exit(1) if $msg == $HEADER_ERROR || $msg == $MESSAGE_ERROR;
                    if ($msg->{type} == TYPE_CONN_ESTAB) {
                        sendm($client, TYPE_CONN_OK, '0'); # with header OK;\
                        exit(0)
                    }
                    elsif ($msg->{type} == TYPE_START_WORK) {
                        # add task to queue by ref
                        my $task_id = $queue->add($msg->{messages});
                        if ($task_id) {
                            kill('ALRM', $worker_manager_pid);
                            sendm($client, TYPE_NEW_WORK_OK, $task_id)
                        }
                        else {
                            sendm($client, TYPE_NEW_WORK_ERR, '0')
                        }
                    }
                    elsif ($msg->{type} == TYPE_CHECK_WORK) {
                        # Get the number of work to check (first elemenet in the list
                        # of messages)
                        my ($status, $filename) = $queue->get_status($msg->{messages}->[0]);
                        if ($status eq 'DONE' || $status eq 'ERROR') {
                            open(my $fh, '<', $filename) or die "Can't open the result file!\n";
                            my @result = <$fh>;
                            chomp foreach @result;
                            if ($status eq 'DONE') {
                                sendm($client, STATUS_DONE, \@result)
                            }
                            else {
                                sendm($client, STATUS_DONE, \@result)
                            }
                            $queue->delete($msg->{messages}->[0])
                        }
                        elsif ($status eq 'NEW') {
                            sendm($client, STATUS_NEW, '0');
                        }
                        elsif ($status eq 'WORK') {
                            sendm($client, STATUS_WORK, '0')
                        }
                        else {
                            sendm($client, TYPE_WORK_ID_ERR, '0')
                        }
                    }
                    else {
                        sendm($client, TYPE_UNKNOWN, '0');
                    }
                    exit(0)
                }

                # --- Something goes wrong, we cannot fork
                else {
                    die "Can't fork: $!";
                }

            }
            else {
               sendm($client, TYPE_CONN_ERR, '0');
               close($client);
               next;
            }
        }
    }
    $socket->close();
    warn "Exit the server. Bye-bye!\n";
    kill('INT', $worker_manager_pid)
};

1;
