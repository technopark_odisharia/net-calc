package Local::Calc::Calc;

use 5.010;
use strict;
use warnings;

our $VERSION = '0.0.1a';

use Exporter 'import';
our @EXPORT = qw(tokenize rpn evaluate);

sub tokenize {
    chomp(my $expr = shift);
    # print STDERR "Expression: $expr\n";
    my @source = grep ( !m/^(\s*|)$/, split m{
            (
                (?<!e) [+-]
                |
                [*()/^]
                |
                \s+
            )
        }x, $expr );
    my @result;

    my $parenthes = 0;
    my $operators = 0;
    my $numbers = 0;
    my $previous = "";
    my $previous_type = "";

    for (@source) {
        if ( $_ =~ m/^[-+]$/ and $previous =~ m/^((\(|\s|)|([\+\-\/\*\^\(]))$/ ) {
            $previous_type = "unary operator";
            push( @result, "U" . $_ );
        }
        elsif ( $_ =~ m/^\d+$/ ) {
            $numbers += 1;
            $previous_type = "number";
            push( @result, "" . $_ )
        }
        elsif ( $_ =~ m/^\d*\.?\d+((e?[-+]?\d+)|(\d*))$/ ) {
            $numbers += 1;
            $previous_type = "number";
            push( @result,  sprintf("%g", $_) );
        }
        else {
            $previous_type = ( $_ =~ m/^([\+\-\*\/\^])$/ ? "operator" : "parenthes" );
            $operators += ( $_ =~ m/^([\+\-\*\/\^])$/ ? 1 : 0 );
            $parenthes += ( $_ =~ m/^\($/ ? 1 : ( $_ =~ m/^\)$/ ? -1 : 0 ) );
            push( @result, $_ );
        }
        $previous = $_;
    }

    if ( !$numbers ) {
        # die "There's no number!";
        return "NaN"
    }

    if ( $parenthes ) {
        # die "Mismatch number of parentheses!";
        return "NaN"
    }

    if (!($numbers == $operators + 1)) {
        # die "Mismatch numbers and operators!"
        return "NaN"
    };

    return \@result;
};

sub rpn {
    my $expr   = shift;
    my $tokens = tokenize($expr);
    return "NaN" if $tokens eq "NaN";
    my @source = @{$tokens};

    my @stack;
    my @rpn;

    my $value = "";

    my %operators = (
        "U-" => { "precedence" => "5", "associativity" => "right" },
        "U+" => { "precedence" => "5", "associativity" => "right" },
        "^"  => { "precedence" => "4", "associativity" => "right" },
        "*"  => { "precedence" => "3", "associativity" => "left" },
        "/"  => { "precedence" => "3", "associativity" => "left" },
        "+"  => { "precedence" => "2", "associativity" => "left" },
        "-"  => { "precedence" => "2", "associativity" => "left" },
    );

    for (@source) {
        if ( $_ =~ m/\d+\.?\d*/ ) {
            push(@rpn, $_);
        }
        elsif ( $_ =~ m/^[\(]$/ ) {
            push(@stack, $_);
        }
        elsif ( $_ =~ m/^[\)]$/ ) {
            while ( $stack[-1] !~ m/^[\(]$/ ) {
                push(@rpn, pop(@stack));
            }
            pop(@stack);
        }
        elsif ( $_ =~ m/^(U[\+\-]|[\+\-\*\/\^])$/ ) {
            while (
                    (
                        (
                            @stack
                        )
                        and
                        (
                            $stack[-1] =~ m/^(U[\+\-]|[\+\-\*\/\^])$/
                        )
                    )
                    and
                    (
                        (
                            ($operators{$_}->{"associativity"} eq "left") and
                            ($operators{$_}->{"precedence"} <= $operators{$stack[-1]}->{"precedence"})
                        )
                        or
                        (
                            ($operators{$_}->{"associativity"} eq "right") and
                            ($operators{$_}->{"precedence"} < $operators{$stack[-1]}->{"precedence"})
                        )
                    )
            ) {
                push(@rpn, pop(@stack));
            };
            push(@stack, $_);
        }
    }
    while (@stack) {
        push(@rpn, pop(@stack));
    }
    return \@rpn;
};

sub evaluate {
    my $rpn   = shift;
    return "NaN" if $rpn eq "NaN";
    my @stack = ();
    my @token = @{$rpn};
    my $res;
    for (@token) {
        if ( $_ eq "U-" ) {
                my $x = pop(@stack);
                push( @stack, -$x );
            }
        elsif ( $_ eq "U+" ) {
            my $x = pop(@stack);
            push( @stack, $x );
        }
        elsif ( $_ =~ /[\+\-\*\/\^]/ ) {
            if ( scalar(@stack) < 2 ) {
                # die 'NaN';
                # exit;
                return "NaN"
            }
            my $x = pop(@stack);
            my $y = pop(@stack);
            if ( $_ eq '*' ) {
                    $res = $y * $x;
                }
            elsif ( $_ eq '/' ) {
                $res = $y / $x;
            }
            elsif ( $_ eq '+' ) {
                $res = $y + $x;
            }
            elsif ( $_ eq '-' ) {
                $res = $y - $x;
            }
            elsif ( $_ eq '^' ) {
                $res = $y ** $x;
            }
            else {
                # die 'NaN';
                # exit;
                return "NaN"
            }

            push( @stack, $res );
        }
        elsif ( $_ =~ /[0-9]/ ) {
            push( @stack, $_ );
        }
        else {
            # die 'NaN';
            # exit;
            return "NaN"
        }
    }
    if (@stack > 1) {
        # die 'NaN';
        # exit;
        return "NaN"
    }
    return pop(@stack);
};

1;
